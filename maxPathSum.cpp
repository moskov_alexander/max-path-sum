#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

// Calculates the maximum path sum of a given triangle
int maxPathSum(std::vector<std::vector<int>> *triangle) 
{
    // Loop through the rows from bottom to top
    for (int i = triangle->size() - 1; i >= 0; i--) 
    {
        // Loop through the columns of each row
        for (int j = 0; j < i; j++) 
        {
            // Compare each pair of numbers in the row and add the larger one to their parent
            if ((*triangle)[i][j] > (*triangle)[i][j + 1]) 
            {
                (*triangle)[i - 1][j] += (*triangle)[i][j];
            }
            else {
                (*triangle)[i - 1][j] += (*triangle)[i][j + 1];
            }
        }
    }

    // The maximum sum can be found at the top of the triangle
    return (*triangle)[0][0];
}

int main (int argc, char *argv[]) 
{
    // Open file from path
    std::ifstream inputFile;
    inputFile.open(argv[1]);

    // Check if file was opened successfully
    if (!inputFile) {
        throw std::runtime_error("Usage: maxPathSum file.txt");
    }
    
    std::vector<std::vector<int>> triangle;
    std::string buffer;
    int number;

    // Parse the file line by line
    while (!inputFile.eof()) 
    {
        std::getline(inputFile, buffer);
        std::stringstream intStream( buffer );
        std::vector<int> vector;

        // Stream the parsed integers into a vector
        while (intStream >> number) 
        {
            vector.push_back(number);
        }

        // Check for empty lines before adding a new row
        if (!vector.empty()) 
        {
            triangle.push_back(vector);
        }
    }
    
    inputFile.close();
    
    int result = maxPathSum(&triangle);
    std::cout << result << std::endl;

    return 0;
}
